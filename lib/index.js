"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "parseWorldStateValue", {
  enumerable: true,
  get: function () {
    return _parseWorldStateValue.default;
  }
});
Object.defineProperty(exports, "blkGraphqlFragment", {
  enumerable: true,
  get: function () {
    return _const.blkGraphqlFragment;
  }
});
Object.defineProperty(exports, "liteBlkGraphqlFragment", {
  enumerable: true,
  get: function () {
    return _const.liteBlkGraphqlFragment;
  }
});
Object.defineProperty(exports, "txGraphqlFragment", {
  enumerable: true,
  get: function () {
    return _const.txGraphqlFragment;
  }
});
Object.defineProperty(exports, "COMBINATION_DELIMITER", {
  enumerable: true,
  get: function () {
    return _const.COMBINATION_DELIMITER;
  }
});
exports.default = void 0;

var _sync = _interopRequireDefault(require("./sync"));

var _syncBlockHandlerOfCustom = require("./syncBlockHandlerOfCustom");

var _log = _interopRequireDefault(require("./log"));

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _parseWorldStateValue = _interopRequireDefault(require("./nodeJava/parseWorldStateValue"));

var _const = require("./const");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright 2019 Linkel Technology Co., Ltd, Beijing
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BA SIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
const [SUBJ_WORLD_STATE, SUBJ_TRANSACTION, SUBJ_BLOCK] = ["worldState", "transaction", "block"];

class RepChainSynchronizer {
  /**
   * Creates an instance of RepChainSynchronizer.
   * 
   * @param {Object} args - 构造参数
   * @param {string} args.url - RepChain url 
   * @param {string} args.subscribeUrl - RepChain订阅url 
   * @param {PrismaClient} args.prisma - prisma client object
   * @param {string} args.logPath - 输出的日志文件路径
   * @param {string} args.startingHeight - 开始同步的区块高度，默认从第1块开始同步
   * @memberof RepChainSynchronizer
   */
  // TODO: Remove url and subscribeUrl
  constructor({
    url,
    subscribeUrl,
    blockchainId,
    prisma,
    logPath,
    startingHeight = 1,
    onNetworkError
  }) {
    this._logger = (0, _log.default)(logPath, blockchainId);
    const syncWorldStateObservable = new _syncBlockHandlerOfCustom.SyncWorldStateObservable();
    const syncTransactionObservable = new _syncBlockHandlerOfCustom.SyncTransactionObservable();
    const syncBlockObservable = new _syncBlockHandlerOfCustom.SyncBlockObservable(); // 同步区块链数据后，构建用户账户信息,证书信息,操作权限信息,授权信息等

    const accountObserver = new _syncBlockHandlerOfCustom.AccountObserver(prisma);
    const certificateObserver = new _syncBlockHandlerOfCustom.CertificateObserver(prisma);
    const operationObserver = new _syncBlockHandlerOfCustom.OperationObserver(prisma);
    const authorizationObserver = new _syncBlockHandlerOfCustom.AuthorizationObserver(prisma);
    const certificateAuthorizationBindingObserver = new _syncBlockHandlerOfCustom.CertificateAuthorizationBindingObserver(prisma);
    const credentialClaimStructObserver = new _syncBlockHandlerOfCustom.CredentialClaimStructObserver(prisma);
    const verifiableCredentialStatusObserver = new _syncBlockHandlerOfCustom.VerifiableCredentialStatusObserver(prisma);
    const chaincodeStateObserver = new _syncBlockHandlerOfCustom.ChaincodeStateObserver(prisma);
    syncWorldStateObservable.subscribe(accountObserver);
    syncWorldStateObservable.subscribe(certificateObserver);
    syncWorldStateObservable.subscribe(operationObserver);
    syncWorldStateObservable.subscribe(authorizationObserver);
    syncWorldStateObservable.subscribe(certificateAuthorizationBindingObserver);
    syncWorldStateObservable.subscribe(credentialClaimStructObserver);
    syncWorldStateObservable.subscribe(verifiableCredentialStatusObserver);
    syncWorldStateObservable.subscribe(chaincodeStateObserver);
    const sync = new _sync.default({
      prisma,
      blockchainId,
      apiUrl: url,
      wsUrl: subscribeUrl,
      customObservable: {
        syncTransactionObservable,
        syncBlockObservable,
        syncWorldStateObservable
      },
      startingHeight,
      logger: this._logger,
      onNetworkError
    });
    this._syncWorldStateObservable = syncWorldStateObservable;
    this._syncTransactionObservable = syncTransactionObservable;
    this._syncBlockObservable = syncBlockObservable;
    this._sync = sync;
    this._prisma = prisma;
    this._blockchainId = blockchainId;
  }
  /**
   * 开启同步，包括周期性的主动同步(默认)和订阅被动同步
   *
   * @param {Object} args - 同步参数
   * @param {boolean} [args.periodicallyPull=true] - 是否开启周期性主动同步，默认开启
   * @param {number} [args.periodicallyPullInterval=13000] - 周期性主动同步时的时间间隔,默认为13000ms
   * @memberof RepChainSynchronizer
   */


  async startSync({
    periodicallyPull = true,
    periodicallyPullInterval = 13000
  } = {}) {
    this._sync.setShouldSynch(true); // 周期性主动拉取同步


    if (periodicallyPull) {
      this._pullSyncTimer = setInterval(() => {
        this._sync.StartPullBlocks();
      }, periodicallyPullInterval);
    } // 实时推送被动同步


    this._sync.startSyncPush();

    await this._prisma.updateNetwork({
      where: {
        id: this._blockchainId
      },
      data: {
        IS_SYNCHRONIZING: true
      }
    });

    this._logger.info("Started blockchain synchronization");
  }
  /**
   * 手动拉取同步
   *
   * @memberof RepChainSynchronizer
   */


  pullSync() {
    this._sync.StartPullBlocks();
  }
  /**
   * 停止同步
   *
   * @memberof RepChainSynchronizer
   */


  async stopSync() {
    this._sync.setShouldSynch(false);

    if (this._pullSyncTimer) {
      clearTimeout(this._pullSyncTimer);
    }

    if (this._sync.repchainEventTube) {
      this._sync.repchainEventTube.close("Unsubscribe push sync");
    }

    await this._prisma.updateNetwork({
      where: {
        id: this._blockchainId
      },
      data: {
        IS_SYNCHRONIZING: false
      }
    });

    this._logger.info("Stoped RepChain synchronization service");
  }
  /**
   * 订阅观察者，以在同步区块链数据后，根据被观察对象的变化执行定制动作
   * 其中，被观察对象有3种：worldState,transaction和block，默认为worldState
   *
   * @param {Observer} observer
   * @memberof RepChainSynchronizer
   */


  subscribeObserver(observer, subject = SUBJ_WORLD_STATE) {
    if (subject === SUBJ_WORLD_STATE) {
      this._syncWorldStateObservable.subscribe(observer);
    } else if (subject === SUBJ_TRANSACTION) {
      this._syncTransactionObservable.subscribe(observer);
    } else if (subject === SUBJ_BLOCK) {
      this._syncBlockObservable.subscribe(observer);
    }
  }
  /**
   * 取消订阅观察者
   *
   * @param {*} observer
   * @memberof RepChainSynchronizer
   */


  unsubscribeObserver(observer) {
    this._syncWorldStateObservable.unSubscribe(observer);

    this._syncBlockObservable.unSubscribe(observer);

    this._syncTransactionObservable.unSubscribe(observer);
  }

  static getDatamodel() {
    return _fs.default.readFileSync(_path.default.join(__dirname, "..", "database", "prisma", "datamodel.gql")).toString();
  }

}

var _default = RepChainSynchronizer;
exports.default = _default;
//# sourceMappingURL=index.js.map