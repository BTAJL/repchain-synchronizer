"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _winston = require("winston");

require("winston-daily-rotate-file");

var _path = _interopRequireDefault(require("path"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const Logger = (logPath, blockchainId) => {
  const indexOfExt = logPath.lastIndexOf(_path.default.extname(logPath));
  const filename = `${logPath.slice(0, indexOfExt)}-%DATE%-${blockchainId}${logPath.slice(indexOfExt)}`;
  const transport = new _winston.transports.DailyRotateFile({
    filename,
    datePattern: "YYYYMMDD",
    zippedArchive: true,
    maxSize: "15m",
    maxFiles: "14d"
  });
  const logger = (0, _winston.createLogger)({
    level: "info",
    format: _winston.format.combine(_winston.format.timestamp(), _winston.format.errors({
      stack: true
    }), _winston.format.label({
      label: "rss"
    }), _winston.format.printf(info => `${info.timestamp} ${info.label} ${info.level} "${info.message}" ${info.stack ? `stack:{ ${info.stack} }` : ""}`)),
    transports: [transport]
  });
  return logger;
};

var _default = Logger;
exports.default = _default;
//# sourceMappingURL=log.js.map