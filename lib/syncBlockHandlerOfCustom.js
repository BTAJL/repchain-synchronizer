"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ChaincodeStateObserver = exports.VerifiableCredentialStatusObserver = exports.CredentialClaimStructObserver = exports.CertificateAuthorizationBindingObserver = exports.AuthorizationObserver = exports.OperationObserver = exports.CertificateObserver = exports.AccountObserver = exports.Observer = exports.SyncBlockObservable = exports.SyncTransactionObservable = exports.SyncWorldStateObservable = void 0;

var _parseWorldStateValue = _interopRequireDefault(require("./nodeJava/parseWorldStateValue"));

var _const = require("./const");

var _lodash = require("lodash");

var _utils = require("./utils");

var _2 = require(".");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const ECODE = "base64";

class SyncBlockchainDataObservable {
  constructor() {
    this.observers = [];
  }

  isSubscribed(observer) {
    return this.observers.includes(observer);
  }

  subscribe(observer) {
    if (!this.isSubscribed(observer)) {
      this.observers.push(observer);
    }
  }

  unSubscribe(observer) {
    const index = this.observers.indexOf(observer);
    if (index > -1) this.observers.splice(index, 1);
  }

  notify(data) {
    return Promise.all(this.observers.map(observer => observer.update(data)));
  }

}

class SyncWorldStateObservable extends SyncBlockchainDataObservable {
  notify(worldState) {
    return Promise.all((0, _lodash.flattenDeep)(this.observers.map(observer => observer.update(worldState))));
  }

}

exports.SyncWorldStateObservable = SyncWorldStateObservable;

class SyncTransactionObservable extends SyncBlockchainDataObservable {
  notify(transaction) {
    return Promise.all(this.observers.map(observer => observer.update(transaction)));
  }

}

exports.SyncTransactionObservable = SyncTransactionObservable;

class SyncBlockObservable extends SyncBlockchainDataObservable {
  notify(block) {
    return Promise.all(this.observers.map(observer => observer.update(block)));
  }

}

exports.SyncBlockObservable = SyncBlockObservable;

class Observer {
  constructor(prisma, filter) {
    this.prisma = prisma;
    this.filter = filter;
  }

  isMatched(worldState) {
    const {
      keyPrefix
    } = this.filter;
    return new RegExp(keyPrefix).test(worldState.KEY);
  } // eslint-disable-next-line class-methods-use-this


  update(data) {
    return Promise.resolve(this.filter);
  }

}

exports.Observer = Observer;

class AccountObserver extends Observer {
  constructor(prisma) {
    super(prisma, {
      keyPrefix: _const.WORLDSTATE_PREFIX_ACCOUNT
    });
  }

  update(worldState) {
    if (!this.isMatched(worldState)) return Promise.resolve(0);
    const accountBuf = Buffer.from(worldState.VALUE, ECODE);
    const account = (0, _parseWorldStateValue.default)(_const.TCN_ACCOUNT, accountBuf);
    const blockchainId = worldState.BLOCKCHAIN.id;
    const id = account.CREDIT_CODE;
    return this.prisma.upsertAccount({
      where: {
        id
      },
      update: { ...account
      },
      create: { ...account,
        id,
        BLOCKCHAIN: {
          connect: {
            id: blockchainId
          }
        }
      }
    });
  }

}

exports.AccountObserver = AccountObserver;

class CertificateObserver extends Observer {
  constructor(prisma) {
    super(prisma, {
      keyPrefix: _const.WORLDSTATE_PREFIX_CERTIFICATE
    });
  }

  async update(worldState) {
    if (!this.isMatched(worldState)) return Promise.resolve(0);
    const certBuf = Buffer.from(worldState.VALUE, ECODE);
    const cert = (0, _parseWorldStateValue.default)(_const.TCN_CERTIFICATE, certBuf);
    const blockchainId = worldState.BLOCKCHAIN.id;
    const accountId = cert.CREDIT_CODE;
    const isExisted = await this.prisma.$exists.account({
      id: accountId
    });
    let account;

    if (isExisted) {
      account = {
        connect: {
          id: accountId
        }
      };
    } else {
      account = {
        create: {
          id: accountId,
          BLOCKCHAIN: {
            connect: {
              id: blockchainId
            }
          },
          CREDIT_CODE: cert.CREDIT_CODE
        }
      };
    }

    const id = `${cert.CREDIT_CODE}${_2.COMBINATION_DELIMITER}${cert.CERT_NAME}`;
    return this.prisma.upsertCert({
      where: {
        id
      },
      update: { ...cert
      },
      create: { ...cert,
        id,
        BLOCKCHAIN: {
          connect: {
            id: blockchainId
          }
        },
        ACCOUNT: account
      }
    });
  }

}

exports.CertificateObserver = CertificateObserver;

class OperationObserver extends Observer {
  constructor(prisma) {
    super(prisma, {
      keyPrefix: _const.WORLDSTATE_PREFIX_OPERATION
    });
  }

  async update(worldState) {
    if (!this.isMatched(worldState)) return Promise.resolve(0);
    const operationBuf = Buffer.from(worldState.VALUE, ECODE);
    const {
      creatorId,
      ...operation
    } = (0, _parseWorldStateValue.default)(_const.TCN_OPERATION, operationBuf);
    const blockchainId = worldState.BLOCKCHAIN.id;
    const id = (0, _utils.constructIdWithBlockchainId)(blockchainId, operation.id);
    await this.prisma.upsertOperation({
      where: {
        id
      },
      update: { ...operation,
        id
      },
      create: { ...operation,
        id,
        BLOCKCHAIN: {
          connect: {
            id: blockchainId
          }
        },
        CREATOR: {
          connect: {
            id: creatorId
          }
        }
      }
    });
    return this.prisma.updateAccount({
      where: {
        id: creatorId
      },
      data: {
        PERMISSIONS: {
          connect: {
            id
          }
        }
      }
    });
  }

}

exports.OperationObserver = OperationObserver;

class AuthorizationObserver extends Observer {
  constructor(prisma) {
    super(prisma, {
      keyPrefix: _const.WORLDSTATE_PREFIX_AUTHORIZATION
    });
  }

  async update(worldState) {
    if (!this.isMatched(worldState)) return Promise.resolve(0);
    const authorizationBuf = Buffer.from(worldState.VALUE, ECODE);
    let {
      id,
      IS_VALID,
      DISABLED_TIME,
      grantorId,
      granteeIds,
      operationIds,
      ...rest
    } = (0, _parseWorldStateValue.default)(_const.TCN_AUTHORIZATION, authorizationBuf);
    const blockchainId = worldState.BLOCKCHAIN.id;
    id = (0, _utils.constructIdWithBlockchainId)(blockchainId, id); // RepChain v2中授权操作逻辑已限定一次只能对单一账户授权

    const granteeId = granteeIds[0]; // RepChain v2中授权操作逻辑已限定一次只能授权单一操作权限

    const operationId = (0, _utils.constructIdWithBlockchainId)(blockchainId, operationIds[0]);
    await this.prisma.upsertAuthorization({
      where: {
        id
      },
      update: {
        IS_VALID,
        DISABLED_TIME
      },
      create: {
        id,
        IS_VALID,
        DISABLED_TIME,
        GRANTOR: {
          connect: {
            id: grantorId
          }
        },
        GRANTEE: {
          connect: {
            id: granteeId
          }
        },
        OPERATION: {
          connect: {
            id: operationId
          }
        },
        GRANTEE_ID_COMB_OPERATION_ID: `${granteeId}${_2.COMBINATION_DELIMITER}${operationId}`,
        BLOCKCHAIN: {
          connect: {
            id: blockchainId
          }
        },
        ...rest
      }
    }); // 检查相应授予者账户是否还将其有效授予给了其他被授予者账户，

    let existed = await this.prisma.$exists.authorization({
      AND: [{
        IS_VALID: true
      }, {
        GRANTOR: {
          id: grantorId
        }
      }, {
        OPERATION: {
          id: operationId
        }
      }]
    }); // 权限被撤销，更新相应账户的相关属性数据

    if (!IS_VALID) {
      // 针对此次被撤销的权限，若相应授予者账户没有将其有效授予给其他被授予者账户，则将该权限从授予者账户的“已授权权限”(PERMISSIONS_FROM)中删除
      if (!existed) {
        await this.prisma.updateAccount({
          where: {
            id: grantorId
          },
          data: {
            PERMISSIONS_FROM: {
              disconnect: {
                id: operationId
              }
            }
          }
        });
      } // 将该权限从被授予者账户的“已有权限”(PERMISSIONS)和“被授予权限”(PERMISSIONS_TO)中删除


      return this.prisma.updateAccount({
        where: {
          id: granteeId
        },
        data: {
          PERMISSIONS: {
            disconnect: {
              id: operationId
            }
          },
          PERMISSIONS_TO: {
            disconnect: {
              id: operationId
            }
          }
        }
      });
    } else {
      if (!existed) {
        await this.prisma.updateAccount({
          where: {
            id: grantorId
          },
          data: {
            PERMISSIONS_FROM: {
              connect: {
                id: operationId
              }
            }
          }
        });
      }

      return this.prisma.updateAccount({
        where: {
          id: granteeId
        },
        data: {
          PERMISSIONS: {
            connect: {
              id: operationId
            }
          },
          PERMISSIONS_TO: {
            connect: {
              id: operationId
            }
          }
        }
      });
    }
  }

}

exports.AuthorizationObserver = AuthorizationObserver;

class CertificateAuthorizationBindingObserver extends Observer {
  constructor(prisma) {
    super(prisma, {
      keyPrefix: _const.WORLDSTATE_PREFIX_CERTAUTHBIND
    });
  }

  async update(worldState) {
    if (!this.isMatched(worldState)) return Promise.resolve(0);
    const bindingBuf = Buffer.from(worldState.VALUE, ECODE);
    const isBinding = (0, _parseWorldStateValue.default)(_const.TCN_CERTIFICATE_AUTHORIZATION_BINDING, bindingBuf);
    if (!isBinding) return;
    const regexp = new RegExp(`${_const.WORLDSTATE_PREFIX_CERTAUTHBIND}(.*)-(${worldState.BLOCKCHAIN.id}.*)\\.(.*)$`);
    const [_, authId, granteeCreditCode, granteeCertName] = worldState.KEY.match(regexp);
    const certificateId = `${granteeCreditCode}${_2.COMBINATION_DELIMITER}${granteeCertName}`;
    const authorizationId = (0, _utils.constructIdWithBlockchainId)(worldState.BLOCKCHAIN.id, authId); // const { OPERATIONS } = await this.prisma.authorization({ id: authorizationId });

    return this.prisma.updateCert({
      where: {
        id: certificateId
      },
      // data: { PERMISSIONS: { connect: OPERATIONS }, BINDED_AUTHORIZATIONS: { connect: { id: authorizationId } } },
      data: {
        BINDED_AUTHORIZATIONS: {
          connect: {
            id: authorizationId
          }
        }
      }
    });
  }

}

exports.CertificateAuthorizationBindingObserver = CertificateAuthorizationBindingObserver;

class CredentialClaimStructObserver extends Observer {
  constructor(prisma) {
    super(prisma, {
      keyPrefix: _const.WORLDSTATE_PREFIX_CRECLASTRUCT
    });
  }

  async update(worldState) {
    if (!this.isMatched(worldState)) return Promise.resolve(0);
    const ccsBuf = Buffer.from(worldState.VALUE, ECODE);
    const ccs = (0, _parseWorldStateValue.default)(_const.TCN_CREDENTIAL_CLAIM_STRUCT, ccsBuf);
    if (!ccs) return;
    const creClaStructId = (0, _utils.constructIdWithBlockchainId)(worldState.BLOCKCHAIN.id, ccs.id);
    return this.prisma.upsertCreClaStruct({
      where: {
        id: creClaStructId
      },
      update: { ...ccs,
        id: creClaStructId
      },
      create: { ...ccs,
        id: creClaStructId,
        BLOCKCHAIN: {
          connect: {
            id: worldState.BLOCKCHAIN.id
          }
        }
      }
    });
  }

}

exports.CredentialClaimStructObserver = CredentialClaimStructObserver;

class VerifiableCredentialStatusObserver extends Observer {
  constructor(prisma) {
    super(prisma, {
      keyPrefix: _const.WORLDSTATE_PREFIX_VERCRESTATUS
    });
  }

  async update(worldState) {
    if (!this.isMatched(worldState)) return Promise.resolve(0);
    const vcsBuf = Buffer.from(worldState.VALUE, ECODE);
    const vcs = (0, _parseWorldStateValue.default)(_const.TCN_VERIFIABLE_CREDENTIAL_STATUS, vcsBuf);
    if (!vcs) return;
    const verCreStatusId = (0, _utils.constructIdWithBlockchainId)(worldState.BLOCKCHAIN.id, vcs.id);
    return this.prisma.upsertVerCreStatus({
      where: {
        id: verCreStatusId
      },
      update: { ...vcs,
        id: verCreStatusId
      },
      create: { ...vcs,
        id: verCreStatusId,
        BLOCKCHAIN: {
          connect: {
            id: worldState.BLOCKCHAIN.id
          }
        }
      }
    });
  }

}

exports.VerifiableCredentialStatusObserver = VerifiableCredentialStatusObserver;

class ChaincodeStateObserver extends Observer {
  constructor(prisma) {
    super(prisma, {
      keyPrefix: _const.WORLDSTATE_PREFIX_CHAINCODE_STATE
    });
  }

  async update(worldState) {
    if (!this.isMatched(worldState)) return Promise.resolve(0);
    const chaincodeStateBuf = Buffer.from(worldState.VALUE, ECODE);
    const chaincodeState = (0, _parseWorldStateValue.default)(_const.TCN_CHAINCODE_STATE, chaincodeStateBuf); // 影响该合约有效状态worldState的合约只能是该合约本身，因此其CHAINCODES只会有单个元素，这里直接使用该CHAINCODE元素的id值

    const chaincodeId = worldState.CHAINCODES[0].id;
    return this.prisma.updateChaincode({
      where: {
        id: chaincodeId
      },
      data: {
        IS_VALID: chaincodeState
      }
    });
  }

}

exports.ChaincodeStateObserver = ChaincodeStateObserver;
//# sourceMappingURL=syncBlockHandlerOfCustom.js.map