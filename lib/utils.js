"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.constructIdWithBlockchainId = void 0;

var _const = require("./const");

// export const constructIdWithBlockchainId = (_, id) => id;
// for the future extension when we need to use blockchainId as the distinguished prefix
const constructIdWithBlockchainId = (blockchainId, id) => `${blockchainId}${_const.COMBINATION_DELIMITER}${id}`;

exports.constructIdWithBlockchainId = constructIdWithBlockchainId;
//# sourceMappingURL=utils.js.map