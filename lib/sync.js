"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _rclink = require("rclink");

var _saveblock = _interopRequireDefault(require("./saveblock"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright  2018 Linkel Technology Co., Ltd, Beijing
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BA SIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
const {
  Block,
  Event
} = _rclink.RCPROTO.rep.proto;

class Syncher {
  constructor({
    prisma,
    apiUrl,
    wsUrl,
    blockchainId,
    customObservable,
    startingHeight,
    logger,
    onNetworkError
  }) {
    this.prisma = prisma;
    this.repchainClient = new _rclink.CLIENT.Client(apiUrl);
    this.repchainEventTube = new _rclink.EVENTS.EventTube(wsUrl, null, null, logger);
    this.storage = new _saveblock.default({
      prisma,
      blockchainId,
      customObservable,
      startingHeight,
      logger
    });
    this.storage.InitStorager();
    this.isPulling = false;
    this.isPushing = false;
    this._logger = logger;
    this.onNetworkError = onNetworkError; // 更新从远程节点处已知的区块和交易总量（不同于已同步的区块和交易数量）

    this.updateBlockAndTxCountAlrearyKnown();
    return this;
  }

  async updateBlockAndTxCountAlrearyKnown() {
    let chainInfo;

    try {
      chainInfo = await this.getRemoteChainInfo();
    } catch (err) {
      this.isPulling = false;

      this._logger.error("Failed to get the remote chain info, for the reason: ", err);

      this._logger.info("The synch process left from pulling");

      await this.onNetworkError(this.changeRepchainClientApiUrl.bind(this), this.changeRepchainEventTubeWsUrl.bind(this), this.repchainEventTube);
      return;
    }

    ;
    const {
      height: hRemote,
      transactionCount
    } = chainInfo || {};

    if (hRemote > this.storage.blockCount) {
      this.storage.setBlockCount(hRemote);
    }

    if (transactionCount > this.storage.transCount) {
      this.storage.setTransactionCount(transactionCount);
    }
  }

  shouldSynch() {
    return this._shouldSync;
  }

  setShouldSynch(shouldSync) {
    this._shouldSync = shouldSync;
  }

  changeRepchainClientApiUrl(apiUrl) {
    if (this.repchainClient) {
      this.apiUrl = apiUrl;
      this.repchainClient = new _rclink.CLIENT.Client(apiUrl);

      this._logger.info(`Changed to connect another peer with Rest url: ${apiUrl}`);
    }
  }

  changeRepchainEventTubeWsUrl(wsUrl) {
    if (this.repchainEventTube) {
      this.repchainEventTube.close(`Change to connect another peer(with url: ${wsUrl}), disconnect current connection`, 4000);
      this.wsUrl = wsUrl;
      this.repchainEventTube = new _rclink.EVENTS.EventTube(wsUrl, this.repchainEventTubeCallback, null, this._logger);

      this._logger.info(`Changed to connect another peer with Websocket url: ${wsUrl}`);
    }
  }

  startSyncPush() {
    const self = this;

    this.repchainEventTubeCallback = evt => {
      if (!this._shouldSync) return; // console.log('+++++++entry evt');

      const msg = Event.decode(Buffer.from(evt.data)); // console.log('$$$$$$$$$$message decode');
      // 出块通知 TODO 确保块内全部交易写入
      // console.log(JSON.stringify(msg));
      // console.log('msg.action='+msg.action+',msg.to='+msg.to);

      if (msg.action === 2 && msg.from !== "Block") {
        // self.storage.setBlockInc();
        // var blk = msg.blk;
        // console.log('########block')
        if (!self.isPulling && !self.isPushing) {
          // console.log('push start');
          self.isPushing = true;
          const hLocal = self.storage.getLastSyncHeight();

          self._logger.info(`The current local height = ${hLocal}`);

          const hRemote = msg.blk.header.height.toNumber();

          self._logger.info(`The remote pushed block height = ${hRemote}`);

          if (hLocal >= hRemote) {
            self.isPushing = false;

            self._logger.info("The remote pushed block height is not larger than the local's");

            self._logger.info("The synch process left from pushing");

            return;
          }

          self.storage.setBlockCount(hRemote);
          self.storage.setTransactionCount(self.storage.transCount + msg.blk.transactions.length);
          self.BlockToStore(Block.encode(msg.blk).finish()).then(res => {
            self.isPushing = false;

            if (res === 1) {
              // 区块已被成功存储
              self._logger.info(`Pushed the block whose height = ${hRemote}`);
            } else {
              // 该区块不是下一个区块，则主动拉取缺失的区块
              self._logger.info("To pull the absent blocks after the failed push");

              self.StartPullBlocks();
            }

            return 0;
          }).catch(err => {
            self.isPushing = false;

            self._logger.error(`Failed to push the block whose height = ${hRemote}, for the reason: `, err);
          });

          self._logger.info(`Pushing the block whose height = ${hRemote}`);
        }
      }
    }; // 启动push方式的实时区块数据同步


    this.repchainEventTube.setCallback(this.repchainEventTubeCallback);
  }

  async StartPullBlocks() {
    // 启动pull方式的区块数据同步
    // console.log(JSON.stringify(this.storage));
    // console.log(this);
    const self = this; // If we are pulling or pushing already, just skip

    if (self.isPulling) {
      self._logger.info("Already in pulling, just skip this operation");

      return;
    }

    if (self.isPushing) {
      self._logger.info("Already in pushing, just skip this operation");

      return;
    }

    this.isPulling = true;

    self._logger.info("The synch process is in pulling");

    const hLocal = this.storage.getLastSyncHeight();

    self._logger.info(`The current local height = ${hLocal}`);

    let chainInfo;

    try {
      chainInfo = await this.getRemoteChainInfo();
    } catch (err) {
      self.isPulling = false;

      self._logger.error("Failed to get the remote chain info, for the reason: ", err);

      self._logger.info("The synch process left from pulling");

      await this.onNetworkError(this.changeRepchainClientApiUrl.bind(this), this.changeRepchainEventTubeWsUrl.bind(this), this.repchainEventTube);
      return;
    }

    ;
    const {
      height: hRemote,
      transactionCount
    } = chainInfo || {};

    if (hRemote > -1) {
      self._logger.info(`The current remote height = ${hRemote}`);

      if (hRemote > hLocal) {
        this.storage.setBlockCount(hRemote);
        this.storage.setTransactionCount(transactionCount);
        const client = this.repchainClient;

        self._logger.info(`Prepare to pull the blocks whose height is from ${hLocal + 1} to ${hRemote}`);

        this.pullBlock(client, hLocal + 1, hRemote);
      } else {
        this.isPulling = false;

        self._logger.info("The remote height is not larger than the local's");

        self._logger.info("The synch process left from pulling");
      }
    } else {
      self.isPulling = false;

      self._logger.error("Got bad chainInfo");

      self._logger.info("The synch process left from pulling");

      await this.onNetworkError(this.changeRepchainClientApiUrl.bind(this), this.changeRepchainEventTubeWsUrl.bind(this), this.repchainEventTube);
    }
  }

  async getRemoteChainInfo() {
    const client = this.repchainClient;

    this._logger.info(`The remote address = ${client.getAddress()}`); // console.log("new ra ");


    const ci = await client.chainInfo(); // console.log("get ci ");

    if (ci && ci.height && ci.totalTransactions) {
      this._logger.info(`Got the current chainInfo: ${JSON.stringify(ci)}`);

      return {
        height: ci.height,
        transactionCount: ci.totalTransactions
      };
    }

    this._logger.info("Got Bad chainInfo");

    return {
      height: -1
    };
  }

  async BlockToStore(res) {
    // console.log("Before", res);
    const blk = Block.decode(res);

    this._logger.info("Got Block Decoded res");
    /* var crypto = require('crypto');
    var sha = crypto.createHash('SHA256');
    var blkHash = sha.update(buf).digest('hex'); */


    return this.storage.saveBlock(blk);
  }

  async pullBlock(client, h, maxh) {
    if (!this._shouldSync) return;
    const self = this;

    self._logger.info(`Pulling the block whose height = ${h}`);

    let res;

    try {
      self._logger.info("Getting Block from remote");

      res = await client.block(h, _rclink.CLIENT.RESPONSE_FORMAT.STREAM);

      self._logger.info("Got Block from remote");
    } catch (err) {
      self.isPulling = false;

      self._logger.error("Failed to get Block from remote, for the reason: ", err);

      self._logger.info("The synch process left from pulling");

      await this.onNetworkError(this.changeRepchainClientApiUrl.bind(this), this.changeRepchainEventTubeWsUrl.bind(this), this.repchainEventTube);
      return;
    }

    let r;

    try {
      r = await self.BlockToStore(res);

      self._logger.info("Got Block Store res");
    } catch (err) {
      self._logger.error(err);

      r = -1;
    }

    if (r === 1) {
      if (h < maxh) {
        self.pullBlock(client, h + 1, maxh);
      } else {
        self.isPulling = false;

        self._logger.info(`Pulled the blocks whose height is up to ${maxh}`);

        self._logger.info("The synch process left from pulling");
      }
    } else {
      // 重试
      self.isPulling = false;

      self._logger.info("The synch process left from pulling");

      self._logger.info("Wait for retry of block pulling");
    }
  }

}

var _default = Syncher;
exports.default = _default;
//# sourceMappingURL=sync.js.map