"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _java = _interopRequireDefault(require("java"));

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _const = require("../const");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// const java = require("java");
// const fs = require("fs");
// const path = require("path");
// const [
//     COMBINATION_DELIMITER,
//     TCN_ACCOUNT,
//     TCN_CERTIFICATE,
//     TCN_OPERATION,
// ] = ["hh", "Account", "Certificate", "Operation"];
const dependencieDir = _path.default.join(__dirname, "jars");

const dependencies = _fs.default.readdirSync(dependencieDir);

dependencies.forEach(element => {
  _java.default.classpath.push(_path.default.join(dependencieDir, element));
});

const KryoInjection = _java.default.import("com.twitter.chill.KryoInjection");

const parseWorldStateValue = (targetClassName, value) => {
  if (!Buffer.isBuffer(value)) {
    throw new TypeError("Bad type of param value, need Buffer");
  }

  const getTimestamp = timestamp => {
    if (timestamp.isEmptySync()) timestamp = undefined;else {
      timestamp = timestamp.getSync().secondsSync().valueOf() * 1000 + timestamp.getSync().nanosSync().valueOf() / 1000000;
      timestamp = new Date(timestamp).toISOString();
    }
    return timestamp;
  };

  const delimiter = "#@#";
  let bytes = [...value];
  bytes = bytes.map(byte => _java.default.newByte(byte));

  const byteArray = _java.default.newArray("byte", bytes);

  switch (targetClassName) {
    case _const.TCN_ACCOUNT:
      {
        const account = KryoInjection.invertSync(byteArray).getSync();
        const res = {
          ACCOUNT_NAME: account.nameSync(),
          CREDIT_CODE: account.creditCodeSync(),
          PHONE: account.mobileSync(),
          PROFILE: account.signerInfoSync(),
          CREATED_TIME: getTimestamp(account.createTimeSync()),
          DISABLED_TIME: getTimestamp(account.disableTimeSync()),
          IS_VALID: account.signerValidSync(),
          VERSION: account.versionSync()
        };
        return res;
      }

    case _const.TCN_CERTIFICATE:
      {
        const certificate = KryoInjection.invertSync(byteArray).getSync();
        const [creditCode, name] = [certificate.idSync().getSync().creditCodeSync(), certificate.idSync().getSync().certNameSync()];
        const certPEM = certificate.certificateSync();
        const res = {
          IS_VALID: certificate.certValidSync(),
          SIGN_ALG: certificate.algTypeSync(),
          CERT_PEM: certPEM,
          TYPE: certificate.certTypeSync().nameSync(),
          CREDIT_CODE: creditCode,
          CERT_NAME: name,
          HASH: certificate.certHashSync(),
          CREATED_TIME: getTimestamp(certificate.regTimeSync()),
          DISABLED_TIME: getTimestamp(certificate.unregTimeSync()),
          VERSION: certificate.versionSync()
        };
        return res;
      }

    case _const.TCN_OPERATION:
      {
        const operationTypes = {
          OPERATE_UNDEFINED: "OPERATION_UNDEFINED",
          OPERATE_CONTRACT: "OPERATION_CONTRACT",
          OPERATE_SERVICE: "OPERATION_SERVICE"
        };
        const operation = KryoInjection.invertSync(byteArray).getSync();
        const res = {
          id: operation.opIdSync(),
          DESCRIPTION: operation.descriptionSync(),
          creatorId: operation.registerSync(),
          IS_PUBLIC: operation.isPublishSync(),
          TYPE: operationTypes[operation.operateTypeSync()],
          ENDPOINT: operation.operateEndpointSync(),
          SERVICES: {
            set: operation.operateServiceNameSync().mkStringSync(delimiter).split(delimiter)
          },
          CONTRACT_FUNCTION: operation.authFullNameSync(),
          CREATED_TIME: getTimestamp(operation.createTimeSync()),
          DISABLED_TIME: getTimestamp(operation.disableTimeSync()),
          IS_VALID: operation.opValidSync(),
          VERSION: operation.versionSync()
        };
        return res;
      }

    case _const.TCN_AUTHORIZATION:
      {
        const transferTypes = {
          TRANSFER_DISABLE: "TRANSFER_DISABLED",
          TRANSFER_ONCE: "TRANSFER_ONCE",
          TRANSFER_REPEATEDLY: "TRANSFER_INFINITELY"
        };
        const authorization = KryoInjection.invertSync(byteArray).getSync();
        const res = {
          id: authorization.idSync(),
          grantorId: authorization.grantSync(),
          granteeIds: authorization.grantedSync().mkStringSync(delimiter).split(delimiter),
          operationIds: authorization.opIdSync().mkStringSync(delimiter).split(delimiter),
          TRANSFER_TYPE: transferTypes[authorization.isTransferSync()],
          CREATED_TIME: getTimestamp(authorization.createTimeSync()),
          DISABLED_TIME: getTimestamp(authorization.disableTimeSync()),
          IS_VALID: authorization.authorizeValidSync(),
          VERSION: authorization.versionSync()
        };
        return res;
      }

    case _const.TCN_CERTIFICATE_AUTHORIZATION_BINDING:
      {
        const isBinding = KryoInjection.invertSync(byteArray).getSync();
        return isBinding;
      }

    case _const.TCN_CREDENTIAL_CLAIM_STRUCT:
      {
        const ccs = KryoInjection.invertSync(byteArray).getSync();
        const attributes = [];

        for (let i = 0; i < ccs.attributesSync().lengthSync(); i++) {
          const attribute = ccs.attributesSync().applySync(i);
          attributes.push({
            NAME: attribute.nameSync(),
            TYPE: attribute.typeSync(),
            REQUIRED: attribute.requiredSync(),
            DESCRIPTION: attribute.descriptionSync()
          });
        }

        const res = {
          id: ccs.idSync(),
          VERSION: ccs.ccsVersionSync(),
          CCS_NAME: ccs.nameSync(),
          DESCRIPTION: ccs.descriptionSync(),
          CREATOR: ccs.creatorSync(),
          CREATED: ccs.createdSync(),
          VALID: ccs.validSync(),
          ATTRIBUTES: {
            set: attributes
          }
        };
        return res;
      }

    case _const.TCN_VERIFIABLE_CREDENTIAL_STATUS:
      {
        const vcs = KryoInjection.invertSync(byteArray).getSync();
        const revokedClaimIndex = vcs.revokedClaimIndexSync().lengthSync() === 0 ? [] : vcs.revokedClaimIndexSync().mkStringSync(delimiter).split(delimiter);
        const res = {
          id: vcs.idSync(),
          STATUS: vcs.statusSync(),
          REVOKED_CLAIM_INDEX: {
            set: revokedClaimIndex
          },
          CREATOR: vcs.creatorSync()
        };
        return res;
      }

    case _const.TCN_CHAINCODE_STATE:
      {
        const chaincodeState = KryoInjection.invertSync(byteArray).getSync();
        return chaincodeState;
      }

    default:
      return KryoInjection.invertSync(byteArray).getSync();
  }
}; // For debug
// let valueBase64 = `AQByZXAucHJvdG8ucmMyLlZlckNyZVN0YXR18wEBZGlkOnJlcDppZGVudGl0eS1uZXQ6MTIxMDAwMDA1bDM1MTIwNDW2ATJlMGJkMmRhLTE2NjgtNGM1OS05MmYyLTE3YzE1M2U0ZTY0sXYBAAFOT1RfQUNUSVbFAQFzY2FsYXBiLlVua25vd25GaWVsZFNl9AGKAQEAATEusA==`;
// let value = Buffer.from(valueBase64, "base64");
// console.log(parseWorldStateValue("VerCreStatus", value));


var _default = parseWorldStateValue;
exports.default = _default;
//# sourceMappingURL=parseWorldStateValue.js.map