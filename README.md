# repchain-synchronizer

#### 介绍
提供基于Prisma 1(version 1.31+, https://v1.prisma.io) 进行持久化的RepChain(v1.3)区块链数据同步功能，并以GraphQL的SDL (Schema Definition Language) 形式提供同步后被持久化的区块链数据结构，上层应用开发者在部署Prisma时需使用该SDL。

#### 软件架构
repchain-synchronizer可对RepChain区块链数据进行同步。
- 通过结合使用主动拉取(Pull)与订阅后被动推送(Push)的方式向RepChain区块链网络同步区块链数据;
- 基于Prisma对区块链数据进行持久化；
- 若上层应用也是基于Prisma与数据库交互以持久化数据，则可直接使用本工具包。

其架构关系如下图所示：

![repchain-synchronizer架构](doc/架构图.png)

#### 安装使用
##### 安装
运行命令
```bash
yarn add repchain-synchronizer
```
或
```bash
npm i --save repchain-synchronizer
```
##### 使用
获取区块链数据结构定义SDL:
```javascript
import RepChainSynchronizer from "repchain-synchronizer";
import fs from "fs";
import path from "path";

const targetPath = path.join(__dirname, "..", "src", "db", "generated", "rsDatamodel.gql");
const repchainDatamodel = RepchainSynchronizer.getDatamodel();
fs.writeFileSync(targetPath, repchainDatamodel)
```
获取到区块链数据结构SDL文件rsDatamodel.gql之后，便可以使用`prisma deploy`命令将其与上层应用的业务数据结构定义一同部署到prisma。

同步RepChain区块链数据：
```javascript
import RepChainSynchronizer from "repchain-synchronizer";
import { prisma } from "../db/generated/prisma-client";

const repchainSynchronizer = new RepChainSynchronizer({
    url: "http://localhost:8081", // 拉取同步的URL
    subscribeUrl: "ws://localhost:8081/event", // 订阅同步的URL(基于websocket)
    blockchainId: "blockchain-test", // 区块链唯一标识
    prisma, // prisma client
    logPath: "Log/repchainSynchronizer.log" // 同步过程中产生的日志文件
    startingHeight: 1000, // 同步的起始高度
});
// 订阅观察者以针对区块链数据的更新来变更应用的业务数据,
// 区块链数据分为3类：worldState,transaction和block，
// 该订阅方法允许指定观察者观察某一类区块链数据的更新，默认观察worldState
repChainSynchronizer.subscribeObserver(new MyObserver(), "worldState");
// 开启RepChain区块链数据同步
repChainSynchronizer.startSync();
```
