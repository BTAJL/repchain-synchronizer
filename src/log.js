import { createLogger, format, transports } from "winston";
import "winston-daily-rotate-file";
import path from "path";

const Logger = (logPath, blockchainId) => {
    const indexOfExt = logPath.lastIndexOf(path.extname(logPath));
    const filename = `${logPath.slice(0, indexOfExt)}-%DATE%-${blockchainId}${logPath.slice(indexOfExt)}`;
    const transport = new (transports.DailyRotateFile)({
        filename,
        datePattern: "YYYYMMDD",
        zippedArchive: true,
        maxSize: "15m",
        maxFiles: "14d",
    }); 
    const logger = createLogger({
        level: "info",
        format: format.combine(
            format.timestamp(),
            format.errors({ stack: true }),
            format.label({ label: "rss" }),
            format.printf(info => `${info.timestamp} ${info.label} ${info.level} "${info.message}" ${info.stack ? `stack:{ ${info.stack} }` : ""}`),
        ),
        transports: [
            transport,
        ],
    });

    return logger;
}

export default Logger;
