/*
 * Copyright 2018-2021 Linkel Technology Co., Ltd, Beijing
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BA SIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
/* eslint-disable no-bitwise */
/* eslint-disable no-await-in-loop */
import { isFinite } from "lodash";
import { BLOCK_HEADER_FLAG, CHAINCODE_TYPES, CLASSIFICATION_TYPES, COMBINATION_DELIMITER, RUN_TYPES, STATE_TYPES, TX_TYPES, worldstateGraphqlFragment } from "./const";
import { constructIdWithBlockchainId } from "./utils";
import { RCPROTO } from "rclink";
import { blkGraphqlFragment, txGraphqlFragment } from ".";

const ECODE = "base64";

class BlockStorager {
    constructor({ prisma, blockchainId, customObservable, startingHeight, logger }) {
        this._logger = logger;
        this.prisma = prisma;
        this.customObservable = customObservable;
        this.blockchainId = blockchainId;
        this._logger.info(`The blockchain id is: ${this.blockchainId}`);

        this.startingHeight = startingHeight;
        this.lastBlockHash = "";
        this.blockCount = 0; // the known remote block count
        this.transCount = 0; // the known remote transaction count
        this.syncedTxCount = 0; // the synced transaction count
        this.syncedHeight = 0; // the synced block count
        this.lastBlockTime = new Date(0);
        this.tps = 0.0;
        return this;
    }

    getLastSyncHeight() {
        return this.syncedHeight;
    }

    getLastBlockHash() {
        return this.lastBlockHash;
    }

    setBlockCount(blkcount) {
        this.blockCount = ~~blkcount;
        this._logger.info(`Set blockcount to ${this.blockCount}`);
    }
    setTransactionCount(transCount) {
        this.transCount = ~~transCount;
        this._logger.info(`Set transactioncount to ${this.transCount}`);
    }

    setBlockInc() {
        this.blockCount = this.blockCount + 1;
        this._logger.info(`Set blockcount to ${this.blockCount}`);
    }

    async InitStorager() {
        let net = null;
        const self = this;
        await this.prisma.network({ id: this.blockchainId })
            .then((result) => {
                if (result != null) {
                    net = result;
                }
                return 0;
            }).catch(err => self._logger.error(
                `When query network with id ${this.blockchainId} in InitStorager method`,
                err,
            ));

        if (net != null) {
            // 欲同步的起始高度比已经同步的高度高1以上，则重置链同步数据
            if (this.startingHeight - 1 > net.SYNCED_HEIGHT) {
                net.STARTING_HEIGHT = this.startingHeight;
                net.SYNCED_HEIGHT = this.startingHeight - 1;
                net.LATEST_BLOCK_HASH = "";
                net.TPS = 0.0;
                net.BLOCK_COUNT = 0;
                net.TRANS_COUNT = 0;
                net.LATEST_BLOCK_TIME = new Date(0);
            }
        } else {
            throw new Error(`The network(id: ${this.blockchainId}) is not existed`);
        }

        if (net != null) {
            if (!isFinite(net.SYNCED_HEIGHT)) {
                this.syncedHeight = 0;
            } else {
                this.syncedHeight = net.SYNCED_HEIGHT;
            }
            if (!isFinite(net.SYNCED_TX_COUNT)) {
                this.syncedTxCount = 0;
            } else {
                this.syncedTxCount = net.SYNCED_TX_COUNT;
            }

            this.lastBlockHash = net.LATEST_BLOCK_HASH;

            if (!isFinite(net.TPS)) {
                this.tps = 0;
            } else this.tps = net.TPS;
        }
        self._logger.info("Inited The storager");
    }

    _convertTimestamp(protobufTimestamp) {
        return protobufTimestamp.seconds * 1000 + protobufTimestamp.nanos / 1000000 - 8 * 3600 * 1000;
    }

    _convertSignature(signature) {
        const accountId = signature.certId.creditCode;
        const certId = `${accountId}${COMBINATION_DELIMITER}${signature.certId.certName}`;
        const signatureConverted = {
            ACCOUNT_ID: accountId, 
            SIGNATURE: Buffer.from(signature.signature).toString(ECODE),
            TIMESTAMP: new Date(this._convertTimestamp(signature.tmLocal)),
            CERT_ID: certId,
        };

        return signatureConverted;
    }

    async saveBlock(blk) {
        this._logger.info(`======= To save the block whose height = ${blk.header.height.toNumber()}, timestamp = ${blk.header.endorsements[0].tmLocal.seconds}.${blk.header.endorsements[0].tmLocal.nanos}`);
        const blkData = {
            id: constructIdWithBlockchainId(this.blockchainId, blk.header.hashPresent.toString(ECODE)),
            BLOCKCHAIN: { connect: { id: this.blockchainId } },
            HEADER: {
                create: {
                    id: constructIdWithBlockchainId(this.blockchainId, BLOCK_HEADER_FLAG + blk.header.hashPresent.toString(ECODE)),
                    BLOCKCHAIN: { connect: { id: this.blockchainId } },
                    VERSION: blk.header.version,
                    HEIGHT: blk.header.height.toNumber(),
                    BLOCKER: blk.header.endorsements[0].certId.creditCode,
                    COMMIT_FOR_TX: Buffer.from(blk.header.commitTx).toString(ECODE),
                    COMMIT_FOR_TX_RESULT: Buffer.from(blk.header.commitTxResult).toString(ECODE),
                    PRESENT_HASH: Buffer.from(blk.header.hashPresent).toString(ECODE),
                    // PREVIOUS_BLOCK: {},
                    // FOLLOWING_BLOCK: {},
                    COMMIT_FOR_STATE: Buffer.from(blk.header.commitState).toString(ECODE),
                    COMMIT_FOR_GLOBAL_STATE: Buffer.from(blk.header.commitStateGlobal).toString(ECODE),
                    EXPIRED_HEIGHT: blk.header.heightExpired.toNumber(),
                    ENDORSEMENTS: { create: blk.header.endorsements.map((value) => this._convertSignature(value)) },
                    TRANS_COUNT: blk.transactions.length,
                    TIMESTAMP: new Date(this._convertTimestamp(blk.header.endorsements[0].tmLocal)),
                }
            }
            // TODO: Add SUPERVISION_TX
        };

        if (blk.header.height.toNumber() !== this.startingHeight) {
            const prevBlkHId = constructIdWithBlockchainId(this.blockchainId,  BLOCK_HEADER_FLAG + blk.header.hashPrevious.toString(ECODE));
            blkData.HEADER.create.PREVIOUS_BLOCK_HEADER = { connect: { id: prevBlkHId } };
        }
        // 判断当前的块是不是能够接到已同步的最新块
        if (blk.header.height.toNumber() === this.startingHeight
            || blk.header.hashPrevious.toString(ECODE) === this.lastBlockHash) {
            // 可以保存,继续检查该块是否存在
            await this.saveBlockForOnly(blkData, blk);
            await this.refreshCache4Async(blk, blkData.hash);
            this._logger.info(`Saved the block whose height = ${this.syncedHeight}`);
            return 1;
        }
        this._logger.info("Check failed, the current block is not the next block, canceled the save operation");
        return 0;
    }

    async refreshCache4Async(blk, blkHash) {
        const netData = this.statisNetData(blk, blkHash);
        // console.log(netData);
        await this.prisma.updateNetwork({
            where: { id: this.blockchainId },
            data: netData,
        });
        this.syncedHeight = netData.SYNCED_HEIGHT;
        this.lastBlockHash = netData.LATEST_BLOCK_HASH;
        this.lastBlockTime = netData.LATEST_BLOCK_TIME;
        this.syncedTxCount = netData.SYNCED_TX_COUNT;
        this.tps = netData.TPS;
    }

    statisNetData(blk) {
        // Calculate the tps for the block
        const blockCreatedAt = this._convertTimestamp(blk.header.endorsements[0].tmLocal);
        const blockTimeSpan = blockCreatedAt - this.lastBlockTime.getTime();
        let tps = 1000 * blk.transactions.length / blockTimeSpan;
        // Use -1 to present infinite tps
        tps = isFinite(tps) ? tps.toFixed(2) : -1;
        tps = parseFloat(tps);
        this._logger.info(`The TPS = ${tps}(blockHeight: ${blk.header.height}, blockTimeSpan: ${blockTimeSpan}ms, blockSize: ${blk.transactions.length})`);

        const netData = {
            SYNCED_HEIGHT: blk.header.height.toNumber(),
            SYNCED_TX_COUNT: this.syncedTxCount + blk.transactions.length,
            BLOCK_COUNT: this.blockCount,
            TRANS_COUNT: this.transCount,
            LATEST_BLOCK_HASH: blk.header.hashPresent.toString(ECODE),
            LATEST_BLOCK_TIME: new Date(blockCreatedAt),
            TPS: tps,
        };
        return netData;
    }

    async saveBlockForOnly(blkData, blk) {
        const isExisted = await this.prisma.$exists.block({ id: blkData.id });
        if (!isExisted) {
            this._logger.info(`Saving the block whose height = ${blk.header.height}`);

            // TODO: 持久化事务性：区块持久化后，交易持久化出现错误，已持久化区块数据应该被回滚
            await this.prisma.createBlock(blkData);
            // update the the previous Block
            if (blk.header.height.toNumber() !== this.startingHeight) {
                const prevBlkHId = constructIdWithBlockchainId(this.blockchainId,  BLOCK_HEADER_FLAG + blk.header.hashPrevious.toString(ECODE));
                await this.prisma.updateBlockHeader(
                    {
                        where: { id: prevBlkHId },
                        data: { FOLLOWING_BLOCK_HEADER: { connect: { id: blkData.HEADER.create.id } } },
                    },
                );
            }

            this._logger.info("To save the transactions");
            await this.saveTranscations(blk, blkData);
            this._logger.info("Saved the transactions");

            await this.saveWorldStates(blk);
            this._logger.info("Saved WorldStates through transaction results");

            const blkInfo = await this.prisma.block({ id: blkData.id }).$fragment(blkGraphqlFragment);

            try {
                await this.customObservable.syncBlockObservable.notify(blkInfo);
            } catch (err) {
                const errMsg = `Callback = ${this.customObservable.syncBlockObservable}, invoke error=${err}`;
                this._logger.error(errMsg);
                throw new Error(errMsg);
            }
        }
    }

    async saveWorldStates(blk) {
        this._logger.info("Saving the WorldStates");
        const txs = blk.transactions;
        const txrs = blk.transactionResults;

        for (let i = 0; i < txrs.length; i++) {
            const tx = txs[i];
            const txr = txrs[i];
            if (txr.statesSet) {
                const { chaincodeName, version } = tx.cid;
                const chaincodeId = constructIdWithBlockchainId(
                    this.blockchainId,
                    `${chaincodeName}${COMBINATION_DELIMITER}${version}`
                );
                for (const key of Object.keys(txr.statesSet)) {
                    const id = key;
                    const isExisted = await this.prisma.$exists.worldState({
                        id,
                    });
                    let worldState;
                    if (isExisted) {
                        worldState = await this.prisma.updateWorldState({
                            where: {
                                id,
                            },
                            data: {
                                TRANSACTIONS: { connect: { id: constructIdWithBlockchainId(this.blockchainId, tx.id) } },
                                CHAINCODES: { connect: { id: chaincodeId } },
                                KEY: key,
                                VALUE: txr.statesSet[key].toString(ECODE),
                            },
                        }).$fragment(worldstateGraphqlFragment);
                    } else {
                        worldState = await this.prisma.createWorldState({
                            id,
                            BLOCKCHAIN: { connect: { id: this.blockchainId } },
                            TRANSACTIONS: { connect: { id: constructIdWithBlockchainId(this.blockchainId, tx.id) } },
                            CHAINCODES: { connect: { id: chaincodeId } },
                            KEY: key,
                            VALUE: txr.statesSet[key].toString(ECODE),
                        }).$fragment(worldstateGraphqlFragment);
                    }

                    try {
                        await this.customObservable.syncWorldStateObservable.notify(worldState);
                    } catch (err) {
                        const errMsg = `Callback = ${this.customObservable.syncWorldStateObservable}, invoke error=${err}`;
                        this._logger.error(errMsg);
                        throw new Error(errMsg);
                    }
                }
            }
            if (txr.statesDel) {
                for (const key of Object.keys(txr.statesDel)) {
                    const id = key;
                    await this.prisma.deleteWorldState({ id });
                }
            }
        }
    }

    async saveTranscations(blk, blkData) {
        const txs = blk.transactions;
        const txrs = blk.transactionResults;
        const blkId = blkData.id;
        this._logger.info("Saving the transactions");
        for (let i = 0; i < txs.length; i++) {
            const tx = txs[i];
            const txData = {
                id: constructIdWithBlockchainId(this.blockchainId, tx.id),
                BLOCKCHAIN: { connect: { id: this.blockchainId } },
                TXID: tx.id,
                TYPE: TX_TYPES[tx.type],
                ORDER_IN_BLOCK: i + 1,
                BLOCK: { connect: { id: blkId } },
                TIMESTAMP: new Date(this._convertTimestamp(tx.signature.tmLocal)),
                GAS_LIMIT: tx.gasLimit,
                OID: tx.oid,
                SIGNATURE: { create: this._convertSignature(tx.signature) },
                TRANSACTION_RESULT: {
                    create: {
                        TXID: tx.id,
                        GET_STATES: txrs[i].statesGet,
                        SET_STATES: txrs[i].statesSet,
                        DEL_STATES: txrs[i].statesDel,
                        CODE: txrs[i].err.code,
                        REASON: txrs[i].err.reason,
                        SUCCESS: txrs[i].err.code === 0,
                        BLOCK: { connect: { id: blkId } },
                    }
                },
            };
            // Add the denormalization data field
            txData.SUCCESS = txData.TRANSACTION_RESULT.create.SUCCESS;
            txData.ACCOUNT_ID = tx.signature.certId.creditCode;
            txData.CERT_NAME = tx.signature.certId.certName;
            txData.CHAINCODE_NAME = tx.cid.chaincodeName;
            txData.CHAINCODE_VERSION = tx.cid.version;

            const chaincodeId = constructIdWithBlockchainId(
                this.blockchainId,
                `${tx.cid.chaincodeName}${COMBINATION_DELIMITER}${tx.cid.version}`
            );
            if (tx.type === RCPROTO.rep.proto.Transaction.Type.CHAINCODE_DEPLOY) {
                txData.CHAINCODE = {
                    create: {
                        id: chaincodeId,
                        BLOCKCHAIN: { connect: { id: this.blockchainId } },
                        CHAINCODE_NAME: tx.cid.chaincodeName,
                        VERSION: tx.cid.version,
                        SOURCE_CODE: tx.spec.codePackage,
                        CODE_TYPE: CHAINCODE_TYPES[tx.spec.cType],
                        RUN_TYPE: RUN_TYPES[tx.spec.rType],
                        STATE_TYPE: STATE_TYPES[tx.spec.sType],
                        LEGAL_PROSE: tx.spec.legalProse,
                        CLASSIFICATION: CLASSIFICATION_TYPES[tx.spec.cclassification],
                        DEPLOYER_ID: tx.signature.certId.creditCode,
                    },
                };
                txData.CHAINCODE_DEPLOY_PARAMS = tx.spec.toJSON();
            } else if (tx.type === RCPROTO.rep.proto.Transaction.Type.CHAINCODE_INVOKE) {
                txData.CHAINCODE = { connect: { id: chaincodeId } };
                txData.CHAINCODE_INVOKE_PARAMS = tx.ipt.toJSON();
            } else if (tx.type === RCPROTO.rep.proto.Transaction.Type.CHAINCODE_SET_STATE) {
                txData.CHAINCODE = { connect: { id: chaincodeId } };
                txData.CHAINCODE_SET_STATE_PARAMS = {
                    STATE: tx.state,
                };
            } else {
                this._logger.warning(`Not supported tx type ${tx.type}`);
            }

            // console.log("---------data---------"+JSON.stringify(txData));
            const txInfo = await this.prisma.createTransaction(txData).$fragment(txGraphqlFragment);
            try {
                await this.customObservable.syncTransactionObservable
                    .notify(txInfo);
            } catch (err) {
                const errMsg = `Callback = ${this.customObservable.syncTransactionObservable}, invoke error=${err}`;
                this._logger.error(errMsg);
                throw new Error(errMsg);
            }
            this._logger.info(`Saved transaction i = ${i},txid = ${tx.id}`);
        }
    }
}

export default BlockStorager;
