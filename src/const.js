export const COMBINATION_DELIMITER = "-COMBDELI-";

export const BLOCK_HEADER_FLAG = "-HEADER-";

export const [ 
    TCN_ACCOUNT, 
    TCN_CERTIFICATE,
    TCN_OPERATION,
    TCN_AUTHORIZATION,
    TCN_CERTIFICATE_AUTHORIZATION_BINDING,
    TCN_CREDENTIAL_CLAIM_STRUCT,
    TCN_VERIFIABLE_CREDENTIAL_STATUS,
    TCN_CHAINCODE_STATE,
] = [
    "Account", 
    "Certificate",
    "Operation",
    "Authorization",
    "CertAuthBinding",
    "CreClaStruct",
    "VerCreStatus",
    "ChaincodeState",
];

const getOid = (oid) => oid === "" ? "_" : oid;
const SYSTEM_ID_MANA_CHAINCODE_WORLDSTATE_PREFIX =  `RdidOperateAuthorizeTPL_${getOid("")}_`;
const SYSTEM_VC_MANA_CHAINCODE_WORLDSTATE_PREFIX =  `RVerifiableCredentialTPL_${getOid("")}_`;
export const WORLDSTATE_PREFIX_ACCOUNT = SYSTEM_ID_MANA_CHAINCODE_WORLDSTATE_PREFIX + "signer-";
export const WORLDSTATE_PREFIX_CERTIFICATE = SYSTEM_ID_MANA_CHAINCODE_WORLDSTATE_PREFIX + "cert-";
export const WORLDSTATE_PREFIX_OPERATION = SYSTEM_ID_MANA_CHAINCODE_WORLDSTATE_PREFIX + "oper-";
export const WORLDSTATE_PREFIX_AUTHORIZATION = SYSTEM_ID_MANA_CHAINCODE_WORLDSTATE_PREFIX + "auth-";
export const WORLDSTATE_PREFIX_CERTAUTHBIND = SYSTEM_ID_MANA_CHAINCODE_WORLDSTATE_PREFIX + "bind-";
export const WORLDSTATE_PREFIX_CRECLASTRUCT = SYSTEM_VC_MANA_CHAINCODE_WORLDSTATE_PREFIX + "ccs-";
export const WORLDSTATE_PREFIX_VERCRESTATUS = SYSTEM_VC_MANA_CHAINCODE_WORLDSTATE_PREFIX + "vcs-";
export const WORLDSTATE_PREFIX_CHAINCODE_STATE = "_STATE";

export const TX_TYPES = ["CHAINCODE_UNDEFINED", "CHAINCODE_DEPLOY", "CHAINCODE_INVOKE", "CHAINCODE_SET_STATE"];
export const CHAINCODE_TYPES = ["CODE_UNDEFINED", "CODE_JAVASCRIPT", "CODE_SCALA", "CODE_VCL_DLL", "CODE_VCL_EXE", "CODE_VCL_WASM", "CODE_WASM"];
export const CLASSIFICATION_TYPES = ["CONTRACT_UNDEFINED", "CONTRACT_SYSTEM", "CONTRACT_CUSTOM"];
export const RUN_TYPES = ["RUN_UNDEFINED", "RUN_SERIAL", "RUN_PARALLEL", "RUN_OPTIONAL"];
export const STATE_TYPES = ["STATE_UNDEFINED", "STATE_BLOCK", "STATE_GLOBAL"];

export const txGraphqlFragment = `
    fragment TransactionWithAffluentInfo on Transaction {
        id
        BLOCKCHAIN { id }
        TYPE
        TXID
        ORDER_IN_BLOCK
        CHAINCODE {
            id
            CHAINCODE_NAME
            VERSION
        }
        CHAINCODE_DEPLOY_PARAMS
        CHAINCODE_INVOKE_PARAMS
        CHAINCODE_SET_STATE_PARAMS
        GAS_LIMIT
        OID
        BLOCK {
            id
            HEADER {
                id
                PRESENT_HASH
                HEIGHT
                TRANS_COUNT
                TIMESTAMP
            }
        }
        SIGNATURE {
            id
            ACCOUNT_ID
            CERT_ID
            TIMESTAMP 
            SIGNATURE
        }
        TIMESTAMP
        SUCCESS
    }
    `;

export const blkGraphqlFragment = `
    fragment blockWithAffluentInfo on Block {
        id
        BLOCKCHAIN { id }
        HEADER {
            id
            HEIGHT
            TRANS_COUNT
            TIMESTAMP
            ENDORSEMENTS {
                id
                ACCOUNT_ID
                CERT_ID
                TIMESTAMP
                SIGNATURE
            }
        }
    }
    `;

export const worldstateGraphqlFragment = `
    fragment worldstateWithAffluentInfo on WorldState {
        id
        BLOCKCHAIN { id }
        KEY
        VALUE
        CHAINCODES {
            id
        }
    }
`