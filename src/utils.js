import { COMBINATION_DELIMITER } from "./const";

// export const constructIdWithBlockchainId = (_, id) => id;
// for the future extension when we need to use blockchainId as the distinguished prefix
export const constructIdWithBlockchainId = (blockchainId, id) => `${blockchainId}${COMBINATION_DELIMITER}${id}`;